origin	git@gitlab.com:cygany/sensible-utils.git (fetch)
origin	git@gitlab.com:cygany/sensible-utils.git (push)
upstream	https://salsa.debian.org/debian/sensible-utils.git (fetch)
upstream	https://salsa.debian.org/debian/sensible-utils.git (push)

PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no WANT_AUTOMAKE=latest debian/rules build binary builddir=$PWD TESTS=

It the build fails, execute this:

autoreconf -f -i

and then retry.
